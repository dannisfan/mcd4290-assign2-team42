"use strict";


// Feature 7

const STORAGE_KEY = "ENG1003-RoomUseList"; 
//let RoomUsageListInstance = JSON.parse(localStorage.getItem(STORAGE_KEY)); 
let RoomUsageListobject = JSON.parse(localStorage.getItem(STORAGE_KEY)); 
let RoomUsageListInstance = new RoomUsageList()
RoomUsageListInstance.initialiseFromRoomPDO2(RoomUsageListobject)

let contentRef = document.getElementById("content"); 
let text = ""; 
 
//sort Room list
RoomUsageListInstance.roomList.sort((a,b) => new Date(b.timeChecked) - new Date(a.timeChecked)) 

//Create divs for each obervations
for(let i=0; i<RoomUsageListInstance.roomList.length; i++) { 
 
     
    let roomUsage = RoomUsageListInstance.roomList[i]; 
    let timeChecked = new Date(roomUsage.timeChecked); 
    let date = timeChecked.getDate(); 
     
    let ampmText; 
    if(timeChecked.getHours()||timeChecked.getHours()!== 24 >= 12){ 
        ampmText = "pm" 
    }else{ 
        ampmText = "am" 
    } 
    
     //finds wether its am or pm
    let time = `${timeChecked.getHours()}:${timeChecked.getMinutes()}:${timeChecked.getSeconds()}${ampmText} ` 
     
    let monthStr = ["Jan.","Feb.","Mar.","Apr.","May","Jun.","Jul.","Aug.","Sep.","Oct.","Nov.","Dec."] 
    let month = monthStr[timeChecked.getMonth()] 
    let lightInstance = roomUsage.lightsOn
    let heatingCoolingInstance = roomUsage.heatingCoolingOn
    
    if(lightInstance === true){
        lightInstance = "On"
    }else{
        lightInstance = "Off"
    }
    
    if(heatingCoolingInstance === true){
        heatingCoolingInstance = "On"
    }else{
        heatingCoolingInstance = "Off"
    }
    //convert boolean to an on or off
    
    ////most address is combine with ",clayton 3170....", subtract these length, the rest is building name.
    let buildingNameLength = RoomUsageListInstance._roomList[i]._address.length-29;
    let buildingName = RoomUsageListInstance._roomList[i]._address.substr(0,buildingNameLength)
    

        text += `<div class="mdl-cell mdl-cell--4-col" id=roomUsage${i}>`
        text +=`<table class="observation-table mdl-data-table mdl-js-data-table mdl-shadow--2dp">`
        text +=`<thead> <tr><th class="mdl-data-table__cell--non-numeric">`
        text +=`<h4 class="date">${date} ${month}</h4>`
        text +=`<h5 >${buildingName}<br />Room ${roomUsage._roomNumber}</h5></th>`
        text +=`</tr></thead><tbody><tr><td class="mdl-data-table__cell--non-numeric">`
        text +=`Time: ${time}<br />Lights: ${lightInstance}<br />`
        text +=`Heating/cooling: ${heatingCoolingInstance}<br />`
        text +=`Seat usage: ${roomUsage._seatsUsed} / ${roomUsage._seatsTotal}<br />`
        text +=`<button class="mdl-button mdl-js-button mdl-button--icon" onclick="deleteObservationAtIndex(${i});">`
        text +=`<i class="material-icons">delete</i></button>`
        text +=`</td></tr></tbody></table></div>`
        //return text
        
}
 
contentRef.innerHTML = text; 
 
    
//Feature 8

/*get class from localStorage
convert object to roomusagelist Object
access list of that object */


let allRooms;//undefined
function retrieveClassInLocalStorage() {
    if (typeof (RoomUsageListInstance) !== "undefined") { //check if anything is in local storage, get everything from room list
        allRooms = RoomUsageListInstance;
        
    } 
    
    else {
        alert("Error: localStorage is not supported by this browser.");
    }
    return allRooms;
}


function deleteObservationAtIndex(id){
    retrieveClassInLocalStorage(RoomUsageListInstance).roomList.splice(id, 1); 
    //deleting observation
        
    localStorage.setItem(STORAGE_KEY, JSON.stringify(allRooms)) 
    //parse new roomlist to localstorage
    window.location.reload();// reload website
}


function searching(){
var input, filter, table, tr, h5, td, i, txtValue,tableTag;
  input = document.getElementById("searchField");
  filter = input.value.toLowerCase();
  table = document.getElementById("content");
  tr = table.getElementsByTagName("tr");
  tableTag=table.getElementsByTagName("table")

  // Loop through all table rows, and hide those who don't match the search query
  for (i = 0; i < tr.length; i++) {
    h5 = tr[i].getElementsByTagName("h5")[0];
    td = tr[i].getElementsByTagName("td")[0];
    if (h5) {
      txtValue = h5.textContent || h5.innerText;
      if (txtValue.toLowerCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
        tr[i+1].style.display = "";
          
      } else {
        tr[i].style.display = "none";
          //set tag style to none, but problem is website will allow the tag occupied the white space.
        tr[i+1].style.display = "none";
      }
    } 
  }
}







