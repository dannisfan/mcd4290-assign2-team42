"use strict";
let buildingBuckect1 = new RoomUsageList()
//loads the RoomUsageList from localStorage in the aggregateBy method
let buildingBuckect= buildingBuckect1.aggregateBy()[1]

let outputArea='';
let outputAreaRef = document.getElementById("content");

//delete the first property(roomlist) in buildingBuckect.
delete buildingBuckect._roomList

for (let prop in buildingBuckect){
    let observationNum = buildingBuckect[prop].length;
    let waste =0;
    let seatsUsed3 = 0;//total seats used across all observations 
    let seatsTotal3 = 0;//total number of seats across all observations 
    let lightsOnNum=0;//the number of observations with lights on
    let heatingCoolingOnNum=0 //the number of observations with heating/cooling on
    let highlighting; 
    
    for(let i=0;i<observationNum;i++){
        if((buildingBuckect[prop][i].seatsUsed===0)&(buildingBuckect[prop][i].lightsOn ||buildingBuckect[prop][i].heatingCoolingOn)){
            waste++;//Summation for how many rooms are wasted.       
        }
        seatsUsed3+=buildingBuckect[prop][i].seatsUsed; 
        //extract data to checkhow many seats in used
        seatsTotal3+=buildingBuckect[prop][i].seatsTotal
        
        
        if(buildingBuckect[prop][i].lightsOn===true){
            lightsOnNum++
        }
        if(buildingBuckect[prop][i].heatingCoolingOn===true){
        heatingCoolingOnNum++
        }
    }
    
    //calculate proportion of seats, lights, heatingcooling systems
    let seatUtilized = (seatsUsed3/seatsTotal3*100).toFixed(1);
    let lightUtilized = (lightsOnNum/observationNum*100).toFixed(1);
    let heatingCoolingUtilized = (heatingCoolingOnNum/observationNum*100).toFixed(1);
    //if waste is greater than 0, use mark tag to highlight the biulding.
    if (waste > 0){
      highlighting = '<mark>' + prop + '</mark>';   
    }else{
      highlighting = prop
    }
    outputArea += '<div class="mdl-cell mdl-cell--4-col">';
    outputArea += '<table class="observation-table mdl-data-table mdl-js-data-table mdl-shadow--2dp">';
    outputArea += '<thead<tr><th class="mdl-data-table__cell--non-numeric">'
    outputArea += '<h4>' + highlighting + '</h4></th></tr></thead>';
    outputArea += '<tbody><tr><td class="mdl-data-table__cell--non-numeric">';
    outputArea += 'Observations: ' + observationNum + '<br/>';
    outputArea += 'Wasteful observations: ' + waste + '<br/>';
    outputArea += 'Average seat utilisation: ' + seatUtilized + '%<br/>';
    outputArea += 'Average lights utilisation: ' + lightUtilized + '%<br />'
    outputArea += 'Average heating/cooling utilisation: ' + heatingCoolingUtilized + '%'
    outputArea += '</td></tr></tbody></table></div>';
    
    
}
outputAreaRef.innerHTML=outputArea;
    
 