"use strict";

window.onload = displayMessages()
function displayMessages(){
            if(localStorage.getItem("forCheck")){
            displayMessage("Entry saved")
        }}

//creates an empty global RoomUsageList
let RoomUsageListInstance1 = new RoomUsageList();

let errorMessages = document.getElementById("errorMessages");

let RoomUsageListFromStorage;
let RoomUsageListObject

//The 'Save' button triggers a function
function saveUsageData()
{
    
    let roomNumber = document.getElementById("roomNumber").value    
    let address = document.getElementById("address").value           
    let seatsUsed = Number(document.getElementById("seatsUsed").value)       
    let seatsTotal = Number(document.getElementById("seatsTotal").value)
    
    //To get the states of the lights and heatingcooling
    let lightsOn = true;
    var lightsBox = document.getElementById('lights');
    if (lightsBox.checked) {
        lightsOn = true;
    } else {
        lightsOn = false
    }
    
    let heatingCoolingOn = true;
    var heatingCoolingBox = document.getElementById('heatingCooling');
    if (heatingCoolingBox.checked) {
        heatingCoolingOn = true;
    } else {
        heatingCoolingOn = false
    }
    
    //a timestamp of when the form was saved (when the instance was created)
    //To get the same form of Date as the testData
    let now =new Date();
    //get Month and Date in two digit format
    var dd = (now.getDate() < 10 ? '0' : '') + now.getDate();                      
    var MM = ((now.getMonth() + 1) < 10 ? '0' : '') + (now.getMonth() + 1); 
    let timeChecked=now.getFullYear()+'-'+MM+'-'+dd+'T'+Date().substr(16,8)+'.000000';
    
    
    //check if each field has the appropriate type of data and provide specified error message
    errorMessages.innerHTML=''
    if (isNaN(Number(roomNumber))){
        errorMessages.innerHTML += 'Roomnumber should be a number!' +"<br>"
    }
    
    if (roomNumber===''){
        errorMessages.innerHTML += 'Roomnumber should be Non-empty!' +"<br>"
    }
    
    if(address === ''){
        errorMessages.innerHTML += 'Address should be Non-empty!'+"<br>"
    }
    
    if(isNaN(Number(address)) === false){
        errorMessages.innerHTML += 'Address should be a string!'+"<br>"   
    }
    
    if(document.getElementById("seatsUsed").value === ''){
        errorMessages.innerHTML += 'seatsUsed should be Non-empty!'+"<br>"
    }
    
    if(document.getElementById("seatsTotal").value === ''){
        errorMessages.innerHTML += 'seatsTotal should be Non-empty!'+"<br>"
    }
    //Number.isInteger(num) will ouput false when num is undefined or num is not a integer  
    if(Number.isInteger(seatsUsed)===false){
        errorMessages.innerHTML += 'SeatsUsed should be a integer!'+"<br>"     
    }
    
    if (Number.isInteger(seatsTotal)===false){
        errorMessages.innerHTML += 'SeatsTotal should be a integer!'+"<br>"
    }
    
    //occupied seats and maximum seats are non-negative
    if(seatsUsed < 0){
        errorMessages.innerHTML += 'SeatsUsed should be greater than 0!'+"<br>"    
    }
    
    if(seatsTotal < 0){
        errorMessages.innerHTML += 'seatsTotal should be greater than 0!'+"<br>"    
    }
    
    //ensures occupied seats does not exceed the maximum
    if(seatsTotal < seatsUsed){
        errorMessages.innerHTML += 'SeatsTotal should be greater than SeatsUsed!'+"<br>"
    }
    
    //where any of these tests fail(errorMessages!==''), the data is NOT saved
    if (errorMessages.innerHTML===''){ 
        
        // checks for the availability of localStorage  
         if (typeof(Storage) !== "undefined") {
             
        // Retrieve the stored JSON string and parse to a variable called RoomUsageListObject.
            RoomUsageListObject = JSON.parse(localStorage.getItem(STORAGE_KEY))

        } else {
            console.log("Error: localStorage is not supported by current browser.");
        }
        
        //creates a RoomUsage class instance for each stored observation
        let newRoomUsageInstance = new RoomUsage(roomNumber,address,lightsOn,heatingCoolingOn,seatsUsed, seatsTotal,timeChecked);
        
        
        if(RoomUsageListObject === null){//for the first observation
            
           //this RoomUsage class instance is added to the global RoomUsageList class instance
            RoomUsageListInstance1.addRoomObservation(newRoomUsageInstance)
            
            // checks for the availability of localStorage  
            if (typeof(Storage) !== "undefined") {
        // Stringify RoomUsageInstance to a JSON string and store this JSON string to local storage 
                localStorage.setItem(STORAGE_KEY,JSON.stringify(RoomUsageListInstance1))
                 } else {
                console.log("Error: localStorage is not supported by current browser.");
            }
              
        } else{
            //       Use this to initialise an new instance of the RoomUsageList class
            RoomUsageListFromStorage = new RoomUsageList()
            RoomUsageListFromStorage.initialiseFromRoomPDO2(RoomUsageListObject)
            
            //adds new observation to the RoomUsageList class instance
            RoomUsageListFromStorage.addRoomObservation(newRoomUsageInstance)    
            //let RoomUsageListInStorageInstance = new RoomUsageListInStorage(RoomUsageListFromStorage)
            
            // checks for the availability of localStorage  
            if (typeof(Storage) !== "undefined") {
        // Stringify RoomUsageListFromStorage to a JSON string and store this JSON string to local storage.
                localStorage.setItem(STORAGE_KEY, JSON.stringify(RoomUsageListFromStorage));
                
                 } else {
                console.log("Error: localStorage is not supported by current browser.");
            }      
        }
        }
        localStorage.setItem('forCheck',true);
    
    //if all the datails are satified, reload html
    let condition = isNaN(Number(roomNumber))===false && roomNumber != '' && address != '' &&isNaN(Number(address)) != false&& document.getElementById("seatsUsed").value != '' && document.getElementById("seatsTotal").value != '' && Number.isInteger(seatsUsed) != false && Number.isInteger(seatsTotal) != false && seatsUsed >= 0 && seatsTotal >= 0 && seatsTotal >= seatsUsed
    
    if(condition){
        location.reload();   
    }

}

//The 'Clear' button on the form triggers a function to reset form data
function clearUsageData(){
    //refresh the page and this sets text fields to blank or restores appropriate default values
    location.reload();
}