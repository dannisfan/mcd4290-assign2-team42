"use strict";
let timeBuckect1 = new RoomUsageList()
//loads the RoomUsageList from localStorage in the aggregateBy method
let timeBuckect= timeBuckect1.aggregateBy()[0]

//delete the first property(roomlist) in timeBuckect.
delete timeBuckect._roomList

let output='';
let outputRef = document.getElementById("content");

for (let prop in timeBuckect){
    for(let i=0;i<timeBuckect[prop].length;i++){
        //get what we need to display for the occupancy page
        let seatsUsed2 = timeBuckect[prop][i].seatsUsed;
        let seatsTotal2 = timeBuckect[prop][i].seatsTotal;
        let Occupancy =(seatsUsed2/seatsTotal2*100).toFixed(1);
        
        // create a new property in each roomusageinstance called OccupancyInBucket and insert occupancy value
        timeBuckect[prop][i].OccupancyInBucket=Occupancy
        
        //change the states of heatingCooling/lights from true/false to on/off
        if (timeBuckect[prop][i].heatingCoolingOn){
           timeBuckect[prop][i].heatingCoolingOn ='On'
        }else{
            timeBuckect[prop][i].heatingCoolingOn ='Off'
        }

        if (timeBuckect[prop][i].lightsOn){
           timeBuckect[prop][i].lightsOn ='On'
        }else{
            timeBuckect[prop][i].lightsOn ='Off'
        }
        
        //get the building name from the address
        let nameLength=timeBuckect[prop][i].address.length-29
        let address2=timeBuckect[prop][i].address
        timeBuckect[prop][i].address=address2.substr(0,nameLength)
        
        //change the timeChecked2 structure ie.from 2018-04-12T15:19:08.000000 to 10/06/2018, 08:09:53                 
        let timeChecked2 = timeBuckect[prop][i].timeChecked
        timeBuckect[prop][i].timeChecked = timeChecked2.substr(8,2)+'/'+timeChecked2.substr(5,2)+'/'+timeChecked2.substr(0,4)+', '+timeChecked2.substr(11,8)
    
}
    //use array.sort to order the observations by hour in the bucket
    //ordered by lowest occupancy
    timeBuckect[prop].sort(function(a,b){return a.OccupancyInBucket-b.OccupancyInBucket})
    
    //adjust the layout of the 'occupancy' page
    output+= '<div class="mdl-cell mdl-cell--4-col">'
        +'<table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">'
        +'<thead><tr><th class="mdl-data-table__cell--non-numeric">'
        +'<h5>Worst occupancy for ' + prop + '</h5>'
        +'</th></tr></thead>'
    
    //at most five of these are displayed for each hour
    let limit;
    if (timeBuckect[prop].length<=5){
        limit=timeBuckect[prop].length
    }else{
        limit=5;
    }
    
    let j=0
    //adjust the layout of the 'occupancy' page
    //these display all the info for those observations
    while(j<limit){
        output+='<tbody><tr><td class="mdl-data-table__cell--non-numeric">'
        +'<div><b>' + timeBuckect[prop][j].address + '; Rm ' + timeBuckect[prop][j].roomNumber + '</b></div>'
        +'<div>Occupancy: ' + timeBuckect[prop][j].OccupancyInBucket + '%</div>'
        +'<div>Heating/cooling: ' + timeBuckect[prop][j].heatingCoolingOn + '</div>'
        +'<div>Lights: ' + timeBuckect[prop][j].lightsOn + '</div>'
        +'<div><font color="grey">'
        +'<i>' + timeBuckect[prop][j].timeChecked + '</i>'
        +'</font></div></td></tr>'
        +'</tbody>'
        j++
     }
    
    output += '</table></div>';
}

outputRef.innerHTML=output