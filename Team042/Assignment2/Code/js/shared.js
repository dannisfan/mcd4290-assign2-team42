"use strict";

class RoomUsage {
    constructor(roomNumber,address,lightsOn,heatingCoolingOn, seatsUsed, seatsTotal, timeChecked){
        this._roomNumber=roomNumber;//string, Non-empty
        this._address=address; //String, Non-empty
        this._lightsOn=lightsOn;//boolean,true or false
        this._heatingCoolingOn=heatingCoolingOn;//boolean
        this._seatsUsed=seatsUsed;//number, Positive integer, less than seatsTotal
        this._seatsTotal=seatsTotal;//number, Positive integer
        this._timeChecked= timeChecked;
    }
    get roomNumber(){
        return this._roomNumber;
      }
    set roomNumber(newRoomNumber){
        this._roomNumber = newRoomNumber;
        }
    get address(){
        return this._address;
      }
    set address(newAddress){
        this._address = newAddress;
      }
    get lightsOn(){
        return this._lightsOn;
      }
    set lightsOn(newLightsOn){
        this._lightsOn = newLightsOn;
      }
    get heatingCoolingOn(){
        return this._heatingCoolingOn;
      }
    set heatingCoolingOn(newHeatingCoolingOn){
        this._heatingCoolingOn = newHeatingCoolingOn;
      }
    get seatsUsed(){
        return this._seatsUsed;
      }
    set seatsUsed(newSeatsUsed){
        this._seatsUsed = newSeatsUsed;
      }
    get seatsTotal(){
        return this._seatsTotal;
      }
    set seatsTotal(newSeatsTotal){
        this._seatsTotal = newSeatsTotal;
      }
    get timeChecked(){
        return this._timeChecked;
}
    set timeChecked(newTimeChecked){
        this._timeChecked = newTimeChecked;
      }
    
    //a working initialiseFromPDO method
    initialiseFromRoomPDO1(roomUsageObject){
        this.address = roomUsageObject._address;// Bulding address
        this.roomNumber = roomUsageObject._roomNumber;// Room numebr
        this.lightsOn = roomUsageObject._lightsOn;// Lights on
        this.heatingCoolingOn = roomUsageObject._heatingCoolingOn;// Air Conditioning / Heating on
        this.seatsUsed = roomUsageObject._seatsUsed;// Number of seats in use
        this.seatsTotal = roomUsageObject._seatsTotal;// Total number of seats in room
        this.timeChecked = roomUsageObject._timeChecked;// Date/ time usage checked
      }
}


class RoomUsageList{
    constructor(){
        this._roomList = [];
    }
    
    get roomList () {
        return this._roomList;
    }
    
     set roomList (newRoomList) {
        this._roomList = newRoomList;
    }
    
    addRoomObservation(RoomObservation){
        this._roomList.push(RoomObservation)
    }
    
    //a working initialiseFromPDO method
    initialiseFromRoomPDO2(roomListPDOObject){
        // Initialise the instance via the mutator methods from the PDO object.
        this.roomList =[]
        
        // Code to inialise the RoomUsageList array
        for(let i=0;i<roomListPDOObject._roomList.length;i++){
            let RoomUsageInstance = new RoomUsage()
            RoomUsageInstance.initialiseFromRoomPDO1(roomListPDOObject._roomList[i])
            this.roomList.push(RoomUsageInstance)
        }
    
  }
     aggregateBy(){
        
        //get room list
        let observationsObject = JSON.parse(localStorage.getItem("ENG1003-RoomUseList"));
        let observations = new RoomUsageList()
        observations.initialiseFromRoomPDO2(observationsObject)
        
//*****************************Time buckect**************************************
        //create timebuckect for every necessary hour.
        let timeBuckect = new RoomUsageList()
        let buildingBuckect = new RoomUsageList()
        timeBuckect["8am"]=[];
        timeBuckect["9am"]=[];
        timeBuckect["10am"]=[];
        timeBuckect["11am"]=[];
        timeBuckect["12pm"]=[];
        timeBuckect["1pm"]=[];
        timeBuckect["2pm"]=[];
        timeBuckect["3pm"]=[];
        timeBuckect["4pm"]=[];
        timeBuckect["5pm"]=[];
        timeBuckect["6pm"]=[];
        

        //push building details into its hour.
        for(let i=0; i<observations._roomList.length; i++){
            if(observations._roomList[i]._timeChecked.substr(11,2)==="08"){
                
                   timeBuckect["8am"].push(observations._roomList[i])
                
            }else if(observations._roomList[i]._timeChecked.substr(11,2)==="09"){

                       timeBuckect["9am"].push(observations._roomList[i])

            }else if(observations._roomList[i]._timeChecked.substr(11,2)==="10"){

                       timeBuckect["10am"].push(observations._roomList[i])

            }else if(observations._roomList[i]._timeChecked.substr(11,2)==="11"){

                       timeBuckect["11am"].push(observations._roomList[i])

            }else if(observations._roomList[i]._timeChecked.substr(11,2)==="12"){

                       timeBuckect["12pm"].push(observations._roomList[i])

            }else if(observations._roomList[i]._timeChecked.substr(11,2)==="13"){

                       timeBuckect["1pm"].push(observations._roomList[i])

            }else if(observations._roomList[i]._timeChecked.substr(11,2)==="14"){

                       timeBuckect["2pm"].push(observations._roomList[i])

            }else if(observations._roomList[i]._timeChecked.substr(11,2)==="15"){

                       timeBuckect["3pm"].push(observations._roomList[i])

            }else if(observations._roomList[i]._timeChecked.substr(11,2)==="16"){

                       timeBuckect["4pm"].push(observations._roomList[i])

            }else if(observations._roomList[i]._timeChecked.substr(11,2)==="17"){

                       timeBuckect["5pm"].push(observations._roomList[i])

            }else if(observations._roomList[i]._timeChecked.substr(11,2)==="18"){

                       timeBuckect["6pm"].push(observations._roomList[i])

                }
//*****************************Time buckect**************************************
            
//*****************************Building buckect**************************************
            let buildingName = observations._roomList[i]._address.length-29
           
            ////most address is combine with ",clayton 3170....", subtract these length, the rest is building name.
            //Building buckect
           //check if there is a relate address name as an Array
            if(buildingBuckect[observations._roomList[i]._address.substr(0,buildingName)] === undefined){
               buildingBuckect[observations._roomList[i]._address.substr(0,buildingName)]=[]
              buildingBuckect[observations._roomList[i]._address.substr(0,buildingName)].push(observations._roomList[i])     
                //push that building details as soon as that buildingbuckect created.
              }

            else{
           //if the building buckect exsist, than push
              buildingBuckect[observations._roomList[i]._address.substr(0,buildingName)].push(observations._roomList[i])

               }
            //*****************************Building buckect**************************************
            }

        //return an array containing 2 buckets in this method
        return [timeBuckect, buildingBuckect]
    }      
}  
